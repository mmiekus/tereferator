import unittest
import subprocess
import sys


class  Simple_test_class:
    def test_add(self):
        process = subprocess.run(['./add', '12', '13'], capture_output=True)
        result = process.stdout.decode('utf-8').strip('\n')
        self.assertEqual(result, str(12 + 13))

    if __name__ == '__main__':
        unittest.main()


def add():
    return int(sys.argv[1]) + int(sys.argv[2])
