test_case () {
  sum=$(./add $1 $2)
  if [ $sum != $3 ]
  then
    exit 1
  fi
}

test_case 2 2 4
test_case 2 -2 0
test_case -100 -100 -200
test_case -5000 10000 5000
test_case 255 1 256
