import unittest
import subprocess


class TestCase(unittest.TestCase):
    def test_add(self):
        for a, b in [(12, 13), (0, 4), (9, 0), (100, 10)]:
            process = subprocess.run(["../../add", str(a), str(b)], capture_output=True)
            result = process.stdout.decode("utf-8").strip("\n")
            self.assertEqual(result, str(a + b))


if __name__ == "__main__":
    unittest.main()
