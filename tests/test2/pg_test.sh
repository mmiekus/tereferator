echo "30" > test_file
add 10 20 > prog_file
diff test_file prog_file || exit -1

echo "-1000000000000" > test_file
add -1000000000001 1 > prog_file
diff test_file prog_file || exit -1