#!/bin/bash

set -e

echo Testing tereferator...

reporoot=$(git rev-parse --show-toplevel)
export PATH="$PATH:$reporoot/bin"

@test "testCzarymary" {
  bash -x ./tests/test1/test_czarymary.sh
}

@test "testMhorod" {
  bash -x ./tests/mhorod/run_tests.sh
}

@test "testPg" {
  bash -x ./tests/pg/pg_test.sh
}

@test "testRekin" {
cd tests/tests_rekin5320 && python3 -m unittest test.py && cd ../..
}

@test "testSidor" {
bash -x ./tests/test-sid0r/test.sh
}

@test "testJJ" {
python3 ./tests/tests_jjakobowska/plik.py 
}

