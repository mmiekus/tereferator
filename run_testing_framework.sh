
reporoot=$(git rev-parse --show-toplevel)
export PATH="$PATH:$reporoot/bin"

./bats-core/bin/bats --tap ./run_tests.sh -F junit > tests.xml

