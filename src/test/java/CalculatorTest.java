import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    @Test
    void add() {
        Calculator calculator = new Calculator();
        for(int i = 0; i < 50; i++) {
            int a = new Random().nextInt();
            int b = new Random().nextInt();
            assertEquals(a + b, calculator.add(a, b));
        }
    }
}